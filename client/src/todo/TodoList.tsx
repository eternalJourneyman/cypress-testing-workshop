import React from "react";
import {deleteTodo, getTodos} from "./api/todoApi";
import {useTodos} from "./TodoProvider";
import {useAsync} from "../useAsync";
import './todoList.css'
import {TodoListItem} from "./TodoListItem";

export const TodoList = () => {
    const {todos, setTodos} = useTodos()

    const {run} = useAsync()

    const deleteTodoRunner = async (id: number) => {
        run(
            deleteTodo(id).then((returnedTodo) => {
                getTodos().then(updatedTodos => {
                    setTodos([...updatedTodos]);
                })
            })
        )
    }

    return todos.length > 0 ? (
        <div className={'todoListContainer'}>
            {todos.map(todo => (
                <TodoListItem key={todo.id} todo={todo} deleteTodo={deleteTodoRunner}/>
            ))}
        </div>
    ) : (
        <EmptyTodoList/>
    )
}

const EmptyTodoList = () => {
    return (
        <h1>You have nothing <span> To</span>do</h1>
    )
}

