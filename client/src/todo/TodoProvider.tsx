import React, {createContext, useContext, useEffect, useState} from "react";
import {getTodos, TodoPreview} from "./api/todoApi";
import {useAsync} from "../useAsync";

export interface TodoContextProps {
    todos: TodoPreview[];
    setTodos: (todos: TodoPreview[]) => void;
}

export const TodoContext = createContext<TodoContextProps>({todos: [], setTodos: () => void 0})
TodoContext.displayName = 'TodoContext'

export const TodoProvider: React.FC = ({children}) => {
    const [todos, setTodos] = useState<TodoPreview[]>([])
    const [loading, setLoading] = useState<boolean>(true)
    const {run} = useAsync()

    useEffect(() => {
        run(getTodos().then(data => {
            setTodos(data)
        })
        .finally(() => {
            setLoading(false)
        }))
    }, [run, todos])

    return <TodoContext.Provider value={{todos, setTodos}}>{!loading && children}</TodoContext.Provider>
}

export const useTodos = () => useContext(TodoContext)
