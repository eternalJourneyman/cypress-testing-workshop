import {deleteTodo, TodoPreview} from "./api/todoApi";
import React, {useState} from "react";

export const TodoListItem = ({todo}: { todo: TodoPreview, deleteTodo: (id: number) => void }) => {
    const [open, setOpen] = useState(false)

    const toggleOpen = (e: any) => {
        e.preventDefault()

        setOpen(!open)
    }

    return (
        <div className={'todoListItem'}>
            <div onClick={(e) => toggleOpen(e)} className='header'>
                <div>Title: {todo.title}</div>
                <div>{open ? '-' : '+'}</div>
            </div>
            {
                open ? (
                        <div className='content'>
                            Description: {
                            todo.description
                        }
                            <button onClick={() => deleteTodo(todo.id)}>Delete</button>
                        </div>
                    ) :
                    null
            }
        </div>
    )
}
