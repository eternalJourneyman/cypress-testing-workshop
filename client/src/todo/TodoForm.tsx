import React, {useState} from "react";
import {Button, TextField} from "@mui/material";
import {createTodo, Todo, TodoPreview} from "./api/todoApi";
import {useTodos} from "./TodoProvider";
import {useAsync} from "../useAsync";
import './todoForm.css'

export const TodoForm = () => {
    const [todo, setTodo] = useState<Todo>(createBlankTodo())
    const {run} = useAsync()
    const {todos, setTodos} = useTodos()

    const saveTodo = async (e: { preventDefault: () => void; }) => {
        e.preventDefault()

        const todoToCreate = {
            title: todo.title,
            description: todo.description,
            completed: todo.completed
        }

        run(
            createTodo(todoToCreate).then((returnedTodo) => {
                setTodo(createBlankTodo())
                setTodos([...todos, returnedTodo as TodoPreview]);
            })
        )
    }

    const updateTodo = (updatedTodo: Partial<TodoPreview>) => {
        setTodo({
            ...todo,
            ...updatedTodo
        })
    }
    const isButtonDisabled = todo.title === '' || todo.description === ''

    return (
        <div className={'todoForm'}>
            <TextField id={'title'} type={"text"} label="title" variant="filled" value={todo.title}
                       onChange={(e) => updateTodo({title: e.target.value})}/>

            <TextField id={'description'} label="description" variant="filled" value={todo.description}
                       onChange={(e) => updateTodo({description: e.target.value})}/>

            <Button  className="disabled" variant="contained" onClick={saveTodo} disabled={isButtonDisabled}>Add Todo</Button>
        </div>
    )
}

const blankTodo: Todo = {
    title: '',
    description: '',
    completed: false
}

const createBlankTodo = () => {
    return blankTodo;
}
