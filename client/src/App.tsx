import React from 'react';
import {Header} from "./Header";
import {TodoForm} from "./todo/TodoForm";
import {TodoList} from "./todo/TodoList";
import './App.css'

function App() {

    return (
        <div className={'todoPageContainer'}>
            <Header title={'Cypress Todos'}/>
            <TodoForm/>
            <TodoList />
        </div>
    );
};

export default App;
