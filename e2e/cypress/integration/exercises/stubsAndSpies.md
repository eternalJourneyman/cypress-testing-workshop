# Exercise 4 - Stubs & Spies

For this test suite you must first uncomment the file then:  
- Not remove any existing code!
- Stubbing out the correct responses will make the existing tests pass
  


####**Docs  
https://docs.cypress.io/api/commands/intercept  

https://docs.cypress.io/api/commands/fixture
