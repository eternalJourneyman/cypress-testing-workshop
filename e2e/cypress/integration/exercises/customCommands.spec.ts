// describe("Exercise 4 - Cypress Custom Commands", () => {
//      /* Find duplicated actions in code */
//      /* Create custom cypress commands */
//      /* Refactor test below  to utilize your new custom command */
//      /* https://docs.cypress.io/api/cypress-api/custom-commands */
//
//     it('Add Shopping List ', () => {
//         cy.visit('/')
//
//         cy.findByRole('textbox', {name: 'title'})
//             .type('Gallon of Milk')
//
//         cy.findByRole('textbox', {name: 'description'})
//             .type('3')
//
//         cy.findByRole('button', {name: 'Add Todo'})
//             .click()
//
//         cy.findByRole('textbox', {name: 'title'})
//             .type('Loaf og Bread')
//
//         cy.findByRole('textbox', {name: 'description'})
//             .type('1')
//
//         cy.findByRole('button', {name: 'Add Todo'})
//             .click()
//
//         cy.findByRole('textbox', {name: 'title'})
//             .type('Honey Nut Cherrios')
//
//         cy.findByRole('textbox', {name: 'description'})
//             .type('7')
//
//         cy.findByRole('button', {name: 'Add Todo'})
//             .click()
//         cy.reload()
//     });
// })
