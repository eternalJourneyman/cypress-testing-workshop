# Exercise 2 - Selectors

For this test suite you must:  
- Locate all the elements on the page

####**Notes

Although the previous exercise may have been fun we will now proceed to make our lives easier...for the most part.  

For this exercise we are going to install @testing-library/cypress  

Run `yarn add  @testing-library/cypress` inside the e2e folder

Open up the `tsconfig.json` located at `e2e/cypress/tsconfig.json`

Uncomment line 10 "@testing-library/cypress". This enables type support for cypress testing library and our custom commands.  

Open up the `commands.ts` located at `e2e/cypress/support/commands.ts`    
Uncomment line 1 "import @testing-library/cypress/add-commands"  

You now have access to selectors from the Cypress Testing Library  
ex. Cy.findByRole
https://testing-library.com/docs/cypress-testing-library/intro/
