describe("Exercise 2 - Cypress Selectors", () => {
    /*
        1) Using Cypress selectors find all elements on page

        For this exercise we are going to need to install @testing-library/cypress
        Run yarn add  @testing-library/cypress inside the e2e folder
        Open up the tsconfig.json located at e2e/cypress/tsconfig.json
        Uncomment line 10 "@testing-library/cypress"
        Open up the commands.ts located at e2e/cypress/support/commands.ts
        Uncomment line 1 "import @testing-library/cypress/add-commands"
        You now have access to selectors from the Cypress Testing Library
        ex. Cy.findByRole
        https://testing-library.com/docs/cypress-testing-library/intro/
    */

    it('Find all page elements', () => {
        cy.visit('/')
        // Get Cypress Todos Header

        //  Get title input

        //  Get description input

        //  Get Add Todo button

        // Get first Todo in list

        // Get second Todo in list
    });
})
