describe("Exercise 1 - Cypress Commands", () => {
    /*   Using cypress commands perform all of the actions listed below */
    /*   https://docs.cypress.io/api/commands/get */

    it('Add a Todo Items', () => {
        cy.visit('/')
        //  1)   Get Cypress Todos Header

        //  2)  Get title input

        //  3) Type the words `Third Todo` into title input

        //  4)   Get description input

        //  5) Type the words `practice workshop` into title input

        //  6)  Get Add Todo button

        //  7)  Click Add Todo button
    });

    it('Deletes Todo Items', () => {
        cy.visit('/')
        //  1)  Get Cypress Todos Header

        //  2)  Get 'Third Todo'

        //  3)  Get Delete button on 'Third Todo'

        //  4)  Click Delete button

        //  5)  Verify 'Third Todo' is no longer in list
    });
})
