# Exercise 1 - Commands

For this exercise your job is complete the tests in `commands.spec.ts`.

https://docs.cypress.io/api/commands/get  

https://docs.cypress.io/api/commands/contains


Using cypress commands perform all of the actions listed below

Test # 1 - Add a todo item to the list
For this test you must:  
    - Complete form  
    - Submit the form  
    - Verify that the item you created appears in the list.  

Test # 2 - Delete a todo item

For this test you must:
    - Complete form  
    - Submit form  
    - Verify the item was created  
    - Delete item   
    - Verify  todo no longer exists.   


