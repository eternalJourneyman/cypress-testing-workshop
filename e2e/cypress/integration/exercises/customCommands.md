# Exercise 3 - Custom Commands

For this test suite you must first uncomment the file then:  
- Find duplicated actions in Cypress code
  - Create Custom Cypress Command(s) to get rid of duplication
- Refactor test to utilize your custom command(s)

####**Docs
https://docs.cypress.io/api/cypress-api/custom-commands

