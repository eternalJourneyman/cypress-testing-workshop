// describe("Exercise 3 - Cypress Stubs and Spies", () => {
//     /* For this exercise you are given 3 failing tests */
//     /* All you must do is stub out the correct responses so that the tests pass */
//     /*  https://docs.cypress.io/api/commands/intercept */
//
//     it('Stub out getTodos route with no todos', () => {
//         cy.intercept('GET', '/api/v1/todos', [])
//         cy.visit('/')
//         cy.findByRole('heading', {name: 'Cypress Todos'}).should('exist')
//
//         cy.findByRole('textbox', {name: 'title'}).should('exist')
//
//         cy.findByRole('textbox', {name: 'description'})
//             .type('Complete Cypress workshop')
//
//         cy.findByRole('button', {name: 'Add Todo'})
//     });
//
//     it('Stub out getTodos route with todos ', () =>  {
//         cy.intercept('GET', '/api/v1/todos', [{
//             id: 1,
//             title: 'Stub SaveTodo #1',
//             description: '',
//         },
//             {
//                 id: 2,
//                 title: 'Stub SaveTodo #1',
//                 description: '',
//             }
//         ])
//         cy.visit('/')
//
//         cy.findByRole('textbox', {name: 'title'})
//
//         cy.findByRole('textbox', {name: 'description'})
//             .type('Complete Cypress workshop')
//
//         cy.findByRole('button', {name: 'Add Todo'})
//
//
//         cy.findByText('Stub SaveTodo #1')
//         cy.findByText('Stub SaveTodo #2')
//     });
//
//     it('Stub out getTodos route and use stubsAndSpies fixture', () =>  {
//         cy.intercept('/api/v1/todos', {fixture: 'stubsAndSpies'})
//         cy.visit('/')
//
//         cy.findByRole('textbox', {name: 'title'})
//         // Type the words `First Todo` into title input
//         //    Get description input
//         cy.findByRole('textbox', {name: 'description'})
//             .type('Complete Cypress workshop')
//
//         cy.findByRole('button', {name: 'Add Todo'})
//
//         cy.findByText('Find Fixture')
//         cy.findByText('Use Fixture')
//     });
// })
//
// // NOTE:
// // https://docs.cypress.io/api/commands/intercept
